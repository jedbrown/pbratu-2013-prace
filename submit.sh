#PBS -l select=1:ncpus=8
#PBS -l walltime=00:05:00
#PBS -q qexp
#PBS -A DD13
#PBS -v LD_LIBRARY_PATH

module add PrgEnv-intel
cd $PBS_O_WORKDIR

echo '## Default'
mpirun -n $NCPUS ./pbratu -da_refine 4 -dm_view -snes_monitor -ksp_converged_reason -snes_mf -lambda 6.8
echo '## GMRES(300)'
mpirun -n $NCPUS ./pbratu -da_refine 4 -dm_view -snes_monitor -ksp_converged_reason -snes_mf -lambda 6.8 -ksp_gmres_restart 300
echo '## MINRES'
mpirun -n $NCPUS ./pbratu -da_refine 4 -dm_view -snes_monitor -ksp_converged_reason -snes_mf -lambda 6.8 -ksp_type minres
echo '## CG'
mpirun -n $NCPUS ./pbratu -da_refine 4 -dm_view -snes_monitor -ksp_converged_reason -snes_mf -lambda 6.8 -ksp_type cg
